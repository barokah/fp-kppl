<%-- 
    Document   : index
    Created on : Dec 26, 2015, 3:13:54 PM
    Author     : samsung
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
<title>Inventaris Laboratorium Pemrograman Sistem Informasi</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.11.0.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Study Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--Google Fonts-->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
	</script>
<script src="js/menu_jquery.js"></script>
</head>
    <body>
<!--header start here-->
<div class="header">
	<div class="container">
		 <div class="header-main">
		 <div class="header-logo">
		 <div class="logo">
		 <a href="index.html"><img src="images/lo1.png" alt="" ></a>
		 </div>
		 <div class="top-nav">
		 <span class="icon"><img src="images/menu.png" alt=""> </span>
		 <ul>
                     <li ><a href="index.jsp">Home</a> </li>
                    <li ><a href="About.jsp" >About </a> </li>
                    <li><a href="Inventaris-LPSI.jsp"  >Inventaris LPSI</a></li>
                    <li><a href="index.jsp"> <img src="images/sisfor.jpg" width="152" height="82"> </a></li>
                    <li><a href="Barang-inventaris.jsp" >Barang Inventaris</a></li>
                    <li><a href="Display-inventaris.jsp" >Display Inventaris </a></li>
                    <li><a href="Login.jsp" >Login</a></li>
		 </ul>
                 <script>
                 $("span.icon").click(function(){
                 $(".top-nav ul").slideToggle(500, function(){
                 });
	         });
                </script>	
                </div>
                <div class="clearfix"> </div>
		</div>
                     
		<div class="top-menu">					
		<ul>
                    <li ><a href="index.jsp">Home</a> </li>
                    <li ><a href="About.jsp" >About </a> </li>
                    <li><a href="index.jsp"> <img src="images/sisfor.jpg" width="152" height="82"> </a></li>
                    <li><a href="Barang-inventaris.jsp" >Barang Inventaris</a></li>
                    <li><a href="Display-inventarisuser.jsp" >Display Inventaris </a></li>
                    <li><a href="Login_user.jsp" >LogOut User</a></li>
                              
		</ul>
		</div>
					
		<div class="bann-bottom">
			<h1>L P S I 2</h1>
			<p>Laboratorium Pemrograman Sistem Informasi  </p>
		</div>
                </div>
   </div>
</div>

<div class="copyright">
	<div class="container">
		<div class="copyright-main">
			   <p> Konstruksi Pemrograman Perangkat Lunak - A || Natascha, Unsa, Mia, Selina, Elisa </p> 
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
</body>
</html>

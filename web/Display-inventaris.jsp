<%-- 
    Document   : Inventaris LPSI
    Created on : Dec 26, 2015, 3:17:45 PM
    Author     : samsung
--%>

<!DOCTYPE html>
<!DOCTYPE html>
<html>
    <%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*"%>


    <head>
        <%
    if ((session.getAttribute("username") == null) || (session.getAttribute("username") == "")) {
%>
You are not logged in<br/>
<a href="Login.jsp">Please Login</a>
<%} else {
%>
Welcome <%=session.getAttribute("username")%>
<%
    }
%>
        <title>Inventaris Laboratorium Pemrograman Sistem Informasi </title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.11.0.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Study Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--Google Fonts-->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
	</script>
<!-- //end-smoth-scrolling -->
<script src="js/menu_jquery.js"></script>
    </head>
    <body>
<!--header start here-->
<div class="header1">
	<div class="container">
		 <div class="header-main">
				<!---->
					<div class="header-logo">
						<div class="logo">
						<a href="index.html"><img src="images/lo1.png" alt="" ></a>
						</div>
						<div class="top-nav">
							<span class="icon"><img src="images/menu.png" alt=""> </span>
							<ul>
								<li ><a href="Display-inventaris.jsp">Display Inventaris</a> </li>
								<li ><a href="Procurement.jsp" >Procurement</a> </li>
								<li><a href="Maintain.jsp"  >Maintain</a></li>
                                                                <li><a href="Form-maintain.jsp"  >Form Maintain</a></li>
								<li><a href="Retire.jsp" >Retire</a></li>
                                                                <li><a href="Report.jsp" >Report</a></li>
								<li><a href="Logout.jsp" >Logout </a></li>
							</ul>
							<!--script-->
						<script>
							$("span.icon").click(function(){
								$(".top-nav ul").slideToggle(500, function(){
								});
							});
					</script>				
				</div>
				<div class="clearfix"> </div>
					</div>
			<!---->
			<div class="top-menu">					
					<ul>
					<li ><a href="Display-inventaris.jsp">Display Inventaris</a> </li>
								<li ><a href="Procurement.jsp" >Procurement</a> </li>
								<li><a href="Maintain.jsp"  >Maintain</a></li>
                                                                <li><a href="Form-maintain.jsp"  >Form Maintain</a></li>
								<li><a href="Retire.jsp" >Retire</a></li>
                                                                <li><a href="Report.jsp" >Report</a></li>
								<li><a href="Logout.jsp" >Logout </a></li>
					</ul>
				</div>
					<!--script-->
	 </div>
   </div>
</div>
<!--header end here-->
<!--about start here-->
<div class="about">
	<div class="container">
		<div class="about-main">
			    <div class="about-top">
					<h2>Daftar Inventaris LPSI 2</h2>
					<p>Laboratorium Pemrograman Sistem Informasi 2</p>
				</div>
                   
				
		</div>
		</div>
   
    
    <%
        try {
//load jdbc driver
Class.forName("com.mysql.jdbc.Driver");

//connect to database
String url = "jdbc:mysql://localhost:3306/kpplbarokah";
Connection koneksi=DriverManager.getConnection(url, "root", "bismillah");


//display table
Statement stmt = koneksi.createStatement();
ResultSet result = stmt.executeQuery("select * from inventaris_habispakai");
 
%>
<center> <table id="tbl-table" border="3%" width="86%" class="order-table">
					<tr>
                                                <td width="10%"><b><center>Nama barang</center></b></td>
                                                <td width="10%"><b><center>Tanggal masuk</center></b></td>
                                                <td width="10%"><b><center>Batas servis</center></b></td>
                                                <td width="10%"><b><center>Satuan servis</center></b></td>
                                                <td width="10%"><b><center>Jumlah barang</center></b></td>
                                                <td width="10%"><b><center>Satuan barang</center></b></td>
                                                                         
						
            </tr>
            <% while (result.next()){%>
            <tr>
                <td><center><%=result.getString(1)%></center></td>
                <td><center><%=result.getString(2)%></center></td>
                <td><center><%=result.getString(3)%></center></td>
                <td><center><%=result.getString(4)%></center></td>
                <td><center><%=result.getString(5)%></center></td>
                <td><center><%=result.getString(6)%></center></td>
            </tr>
            <% } %>
            <%
 result.close();
 stmt.close();
 koneksi.close();
 }
 catch (Exception ex)
 {
 out.println ("database not connected");
 }
 
 %>
    </table></center>
</div>
<div class="copyright">
	<div class="container">
		<div class="copyright-main">
			   <p> Konstruksi Pemrograman Perangkat Lunak - A || Natascha, Unsa, Mia, Selina, Elisa </p> 
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!--copyright end here-->
</body>
</html>

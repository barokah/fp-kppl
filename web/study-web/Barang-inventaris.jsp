<%-- 
    Document   : Barang Inventaris
    Created on : Dec 26, 2015, 3:18:49 PM
    Author     : samsung
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
<title>Inventaris Laboratorium Pemrograman Sistem Informasi</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.11.0.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Study Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--Google Fonts-->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
	</script>
<!-- //end-smoth-scrolling -->
<script src="js/menu_jquery.js"></script>
<!--script-->
	<script src="js/modernizr.custom.97074.js"></script>
<script src="js/jquery.chocolat.js"></script>
		<link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
		<!--light-box-files -->
		<script type="text/javascript" charset="utf-8">
		$(function() {
			$('.gallery a').Chocolat();
		});
		</script>
		<script type="text/javascript" src="js/jquery.hoverdir.js"></script>

</head>
<body>
<!--header start here-->
<div class="header1">
	<div class="container">
		 <div class="header-main">
				<!---->
					<div class="header-logo">
						<div class="logo">
						<a href="index.html"><img src="images/lo1.png" alt="" ></a>
						</div>
						<div class="top-nav">
							<span class="icon"><img src="images/menu.png" alt=""> </span>
							<ul>
								<li ><a href="Index-home.jsp">Home</a> </li>
								<li ><a href="About.jsp" >About </a> </li>
								<li><a href="Inventaris-LPSI.jsp"  >Inventaris LPSI</a></li>
								<li><a href="Barang-inventaris.jsp" >Barang Inventaris</a></li>
								<li><a href="Contact.jsp" >Contact Admin </a></li>
							</ul>
							<!--script-->
						<script>
							$("span.icon").click(function(){
								$(".top-nav ul").slideToggle(500, function(){
								});
							});
					</script>				
				</div>
				<div class="clearfix"> </div>
					</div>
			<!---->
			<div class="top-menu">					
					<ul>
						<li ><a href="Index-home.jsp">Home</a> </li>
						<li ><a href="About.jsp" >About </a> </li>
						<li><a href="Inventaris-LPSI.jsp"  >Inventaris LPSI</a></li>
						<li><a href="Index-home.jsp"> <img src="images/sisfor.jpg" width="152" height="82"> </a></li>
						<li><a href="Barang-inventaris.jsp" >Barang Inventaris</a></li>
						<li><a href="Contact.jsp" >Contact Admin </a></li>
                                                
						<li><div class="header-login">
									 <div class="top-nav-right">
										<div id="loginContainer"><a href="#" id="loginButton"><span>Login</span></a>
											    <div id="loginBox">                
											        <form id="loginForm">
											                <fieldset id="body">
											                	<fieldset>
											                          <label for="email">Email Address</label>
											                          <input type="text" name="email" id="email">
											                    </fieldset>
											                    <fieldset>
											                            <label for="password">Password</label>
											                            <input type="password" name="password" id="password">
											                     </fieldset>
											                    <input type="submit" id="login" value="Sign in">
											                	<label for="checkbox"><input type="checkbox" id="checkbox"> <i>Remember me</i></label>
											            	</fieldset>
											            <span><a href="#">Forgot your password?</a></span>
												 </form>
									        </div>
									  </div>
								   </div>
		                         </div>	</li>
					</ul>
				</div>
					<!--script-->
	 </div>
   </div>
</div>
<!--header end here-->
<!--gallery-starts-->
	<div class="gallery">
	<div class="container">
			<div class="gallery-top heading">
				<h2>Our Gallery</h2>
				<p>Berikut merupakan aset-aset inventaris yang ada pada Laboratorium Pemrograman Sistem Informasi</p>
			</div>
			<section>
				<ul id="da-thumbs" class="da-thumbs">
                                    
					<li>
					<a href="images/monitor.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
					<img src="images/monitor.jpg" width="112" height="82" class="img-responsive">
					<div>
					<h5>Monitor</h5>
                                        <span> Monitor yang ada di LPSI terdiri dari monitor LCD Digitech dan LG</span>
					</div>
					</a>
					</li>
                                        
					<li>
					<a href="images/cpu.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
					<img src="images/cpu.jpg" width="250" height="290">
					<div>
					<h5>CPU</h5>
					<span>CPU yang ada di LPSI terdiri dari CPU merk Digitech</span>
					</div>
					</a>
					</li>
                                        
					<li>
					<a href="images/keyboard.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
					<img src="images/keyboard.jpg" width="250" height="290">
					<div>
					<h5>Keyboard</h5>
					<span>Keyboard yang ada di LPSI digunakan untuk setiap monitor yang ada dengan merk Digitech</span>
					</div>
					</a>
					</li>
                                        
					<li>
					<a href="images/mouse.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
					<img src="images/mouse.jpg" width="250" height="290" >
							<div>
								<h5>Mouse</h5>
								<span>Mouse yang ada di LPSI digunakan untuk setiap monitor yang ada dengan merk Digitech</span>
							</div>
						</a>
					</li>
                                        
					<li>	
					<a href="images/kursi.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
							<img src="images/kursi.jpg" width="250" height="290">
							<div>
								<h5>Kursi</h5>
								<span>Kursi digunakan di setiap meja yang memiliki komputer dan meja yang tidak memilik komputer</span>
							</div>
						</a>
					</li>
                                        
					<li>
						<a href="images/meja.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
							<img src="images/meja.jpg" width="250" height="350">
							<div>
								<h5>Meja Komputer</h5>
								<span>Meja digunakan untuk meletakkan monitor LCD</span>
							</div>
						</a>
					</li>
                                        <br>
					<li>
						<a href="images/loker.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
							<img src="images/loker.jpg" width="250" height="290">
							<div>
								<h5>Loker</h5>
								<span>Loker digunakan untuk menyimpan barang-barang mahasiswa sebelum memasuki laboratorium</span>
							</div>
						</a>
					</li>
                                        
					<li>
						<a href="images/proyektor.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
							<img src="images/proyektor.jpg" width="250" height="290">
							<div>
								<h5>Proyektor</h5>
								<span>Proyektor digunakan untuk menampilkan informasi di setiap pembelajaran</span>
							</div>
						</a>
					</li>
                                        
					<li>
						<a href="images/layar.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
							<img src="images/layar.jpg" width="250" height="290">
							<div>
								<h5>Layar Proyektor</h5>
								<span>Layar proyektor digunakan untuk menampilkan informasi di setiap pembelajaran</span>
							</div>
						</a>
					</li>
                                        
                                        <li>
						<a href="images/ac.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
							<img src="images/ac.jpg" width="250" height="290">
							<div>
								<h5>AC</h5>
								<span>AC berguna untuk pendingin ruangan </span>
							</div>
						</a>
					</li>
                                        
                                        <li>
						<a href="images/whiteboard.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
							<img src="images/whiteboard.png" width="250" height="290">
							<div>
								<h5>White Board</h5>
								<span>White board digunakan untuk mendukung proses perkuliahan dan menuliskan informasi-informasi penting</span>
							</div>
						</a>
					</li>
                                        
                                        <li>
						<a href="images/pengharum.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
							<img src="images/pengharum.jpg" width="250" height="290">
							<div>
								<h5>Pengharum Ruangan</h5>
								<span>Pengharum Ruangan berguna untuk mencegah timbulnya bau tidak sedap</span>
							</div>
						</a>
					</li>
                                        
                                        <li>
						<a href="images/spidol.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
							<img src="images/spidol.jpg" width="250" height="290">
							<div>
								<h5>Spidol</h5>
								<span>Spidol digunakan untuk menulis pada whiteboard yang ada</span>
							</div>
						</a>
					</li>
                                        
                                        <li>
						<a href="images/penghapus.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
							<img src="images/penghapus.jpg" width="250" height="290">
							<div>
								<h5>Penghapus Papan Tulis</h5>
								<span>Penghapus yang digunakan untuk menghapus tulisan spidol pada whiteboard</span>
							</div>
						</a>
					</li>
                                        
                                        <li>
						<a href="images/lampu.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
							<img src="images/lampu.jpg" width="250" height="290">
							<div>
								<h5>Lampu</h5>
								<span>Sebagai penerangan pada Laboratorium Pemrograman SIs</span>
							</div>
						</a>
					</li>
					<div class="clearfix"> </div>
				</ul>
			</section>
				
		<script type="text/javascript">
			$(function() {
			
				$(' #da-thumbs > li ').each( function() { $(this).hoverdir(); } );

			});
		</script>
        </div>
	</div>

<!--footer end here-->
<!--copyright start here-->
<div class="copyright">
	<div class="container">
		<div class="copyright-main">
			   <p> Konstruksi Pemrograman Perangkat Lunak - A || Natascha, Unsa, Mia, Selina, Elisa </p> 
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!--copyright end here-->
</body>
</html>

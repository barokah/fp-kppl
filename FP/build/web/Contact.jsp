<%-- 
    Document   : Contact
    Created on : Dec 26, 2015, 3:19:35 PM
    Author     : samsung
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
   <head>
       
<title>Inventaris Laboratorium Pemrograman Sistem Informasi </title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.11.0.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Study Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--Google Fonts-->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
	</script>
<!-- //end-smoth-scrolling -->
<script src="js/menu_jquery.js"></script>
</head>
<body>
<!--header start here-->
<div class="header1">
	<div class="container">
		 <div class="header-main">
				<!---->
					<div class="header-logo">
						<div class="logo">
						<a href="index.html"><img src="images/lo1.png" alt="" ></a>
						</div>
						<div class="top-nav">
							<span class="icon"><img src="images/menu.png" alt=""> </span>
							<ul>
                    <li ><a href="index.jsp">Home</a> </li>
                    <li ><a href="About.jsp" >About </a> </li>
                    <li><a href="Contact.jsp" >Contact Admin </a></li>
                     <li><a href="index.jsp"> <img src="images/sisfor.jpg" width="152" height="82"> </a></li>
                    <li><a href="Login.jsp" >Login Admin</a></li>
                    <li><a href="Login_user.jsp" >Login User</a></li>
                    <li><a href="Signup.jsp" >Sign Up User</a></li
							</ul>
							<!--script-->
						<script>
							$("span.icon").click(function(){
								$(".top-nav ul").slideToggle(500, function(){
								});
							});
					</script>				
				</div>
				<div class="clearfix"> </div>
					</div>
			<!---->
			<div class="top-menu">					
					<ul>
                   <li ><a href="index.jsp">Home</a> </li>
                    <li ><a href="About.jsp" >About </a> </li>
                    <li><a href="Contact.jsp" >Contact Admin </a></li>
                     <li><a href="index.jsp"> <img src="images/sisfor.jpg" width="152" height="82"> </a></li>
                    <li><a href="Login.jsp" >Login Admin</a></li>
                    <li><a href="Login_user.jsp" >Login User</a></li>
                    <li><a href="Signup.jsp" >Sign Up User</a></li
					</ul>
				</div>
					<!--script-->
	 </div>
   </div>
</div>
<!--header end here-->
<!--contact start here-->
<div class="contact">
		<div class="container">
			<div class="contact-top">
				<h2>Contact Us</h2>
				<p>Untuk melakukan peminjaman inventaris ataupun menanyakan jadwal Laboratorium Pemrograman Sistem Informasi silahkan menghubungi admin di bawah ini</p>
			</div>
			<div class="map">
				<h1>GET IN TOUCH</h1>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.670903955781!2d112.79001571456104!3d-7.278236194746647!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fa16b145e469%3A0x83df57e6a93ef2c2!2sJurusan+Sistem+Informasi+ITS!5e0!3m2!1sen!2s!4v1451117172155" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
                    
                    <div class="team-mem">
			 <h1>Administrator</h1>
                         <section>
                         <ul id="da-thumbs" class="da-thumbs">                
                        <li>
			<div class="col-md-3 team-grid">
                            <img src="images/rowi.jpg" width="172" height="172">
                            <h4><b><right> Rowi Fajar 2012</right> </b></h4>
                        </li>
                        <li>
                    <div class="col-md-3 team-grid">
				<img src="images/rizki.jpg"  width="172" height="172" >
                                <h4><b><center> Rizki Nugraha 2012 </center></b></h4>
			</div>
                        </li>
                        
                        <li>
                             <div class="col-md-3 team-grid">
				<img src="images/rizki.jpg"  width="172" height="172" >
                                <h4><b><center> Ali Mansur 2012 </center></b></h4>
			</div>
                        </li>
                        </section>
                                </ul>
			
		   <div class="clearfix"> </div>
		</div>	s
			
			
		</div>
	</div>
<!--contact end here-->
<!--footer start here-->

<!--footer end here-->
<!--copyright start here-->
<div class="copyright">
	<div class="container">
		<div class="copyright-main">
			   <p> Konstruksi Pemrograman Perangkat Lunak - A || Natascha, Unsa, Mia, Selina, Elisa </p> 
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!--copyright end here-->
</body>
</html>

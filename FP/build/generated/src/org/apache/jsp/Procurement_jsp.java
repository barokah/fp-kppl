package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;

public final class Procurement_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    \n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("    <head>\n");
      out.write("        ");

    if ((session.getAttribute("username") == null) || (session.getAttribute("username") == "")) {

      out.write("\n");
      out.write("You are not logged in<br/>\n");
      out.write("<a href=\"Login.jsp\">Please Login</a>\n");
} else {

      out.write("\n");
      out.write("Welcome ");
      out.print(session.getAttribute("username"));
      out.write('\n');

    }

      out.write("\n");
      out.write("        <title>Inventaris Laboratorium Pemrograman Sistem Informasi </title>\n");
      out.write("<link href=\"css/bootstrap.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\">\n");
      out.write("<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->\n");
      out.write("<script src=\"js/jquery-1.11.0.min.js\"></script>\n");
      out.write("<!-- Custom Theme files -->\n");
      out.write("<link href=\"css/style.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\"/>\n");
      out.write("<!-- Custom Theme files -->\n");
      out.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n");
      out.write("<meta name=\"keywords\" content=\"Study Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, \n");
      out.write("Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design\" />\n");
      out.write("<script type=\"application/x-javascript\"> addEventListener(\"load\", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>\n");
      out.write("<!--Google Fonts-->\n");
      out.write("<!-- start-smoth-scrolling -->\n");
      out.write("<script type=\"text/javascript\" src=\"js/move-top.js\"></script>\n");
      out.write("<script type=\"text/javascript\" src=\"js/easing.js\"></script>\n");
      out.write("\t<script type=\"text/javascript\">\n");
      out.write("\t\t\tjQuery(document).ready(function($) {\n");
      out.write("\t\t\t\t$(\".scroll\").click(function(event){\t\t\n");
      out.write("\t\t\t\t\tevent.preventDefault();\n");
      out.write("\t\t\t\t\t$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);\n");
      out.write("\t\t\t\t});\n");
      out.write("\t\t\t});\n");
      out.write("\t</script>\n");
      out.write("<!-- //end-smoth-scrolling -->\n");
      out.write("<script src=\"js/menu_jquery.js\"></script>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("<!--header start here-->\n");
      out.write("<div class=\"header1\">\n");
      out.write("\t<div class=\"container\">\n");
      out.write("\t\t <div class=\"header-main\">\n");
      out.write("\t\t\t\t<!---->\n");
      out.write("\t\t\t\t\t<div class=\"header-logo\">\n");
      out.write("\t\t\t\t\t\t<div class=\"logo\">\n");
      out.write("\t\t\t\t\t\t<a href=\"index.html\"><img src=\"images/lo1.png\" alt=\"\" ></a>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"top-nav\">\n");
      out.write("\t\t\t\t\t\t\t<span class=\"icon\"><img src=\"images/menu.png\" alt=\"\"> </span>\n");
      out.write("\t\t\t\t\t\t\t<ul>\n");
      out.write("\t\t\t\t\t<li ><a href=\"Display-inventaris.jsp\">Inventaris</a> </li>\n");
      out.write("\t\t\t\t\t\t\t\t<li ><a href=\"Procurement.jsp\" >Procurement</a> </li>\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"Maintain.jsp\"  >Maintain</a></li>\n");
      out.write("                                                                <li><a href=\"index.jsp\"> <img src=\"images/sisfor.jpg\" width=\"172\" height=\"102\"> </a></li>\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("                                                                <li><a href=\"Report.jsp\" >Report</a></li>\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"Logout.jsp\" >Logout </a></li>\n");
      out.write("                                                                <li><a href=\"Form-maintain.jsp\"  >Form Maintain</a></li>\n");
      out.write("                                                                <li><a href=\"Add-inventaris.jsp\"  >Form Inventaris</a></li>\n");
      out.write("                                                                <li><a href=\"Form-pengadaan.jsp\"  >Form Pengadaan</a></li>\n");
      out.write("\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t<!--script-->\n");
      out.write("\t\t\t\t\t\t<script>\n");
      out.write("\t\t\t\t\t\t\t$(\"span.icon\").click(function(){\n");
      out.write("\t\t\t\t\t\t\t\t$(\".top-nav ul\").slideToggle(500, function(){\n");
      out.write("\t\t\t\t\t\t\t\t});\n");
      out.write("\t\t\t\t\t\t\t});\n");
      out.write("\t\t\t\t\t</script>\t\t\t\t\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<div class=\"clearfix\"> </div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t<!---->\n");
      out.write("\t\t\t<div class=\"top-menu\">\t\t\t\t\t\n");
      out.write("\t\t\t\t\t<ul>\n");
      out.write("\t\t\t\t\t<li ><a href=\"Display-inventaris.jsp\">Inventaris</a> </li>\n");
      out.write("\t\t\t\t\t\t\t\t<li ><a href=\"Procurement.jsp\" >Procurement</a> </li>\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"Maintain.jsp\"  >Maintain</a></li>\n");
      out.write("                                                                <li><a href=\"index.jsp\"> <img src=\"images/sisfor.jpg\" width=\"172\" height=\"102\"> </a></li>\n");
      out.write("\t\t\t\t\t\t\t\n");
      out.write("                                                                <li><a href=\"Report.jsp\" >Report</a></li>\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"Logout.jsp\" >Logout </a></li>\n");
      out.write("                                                                <li><a href=\"Form-maintain.jsp\"  >Form Maintain</a></li>\n");
      out.write("                                                                <li><a href=\"Add-inventaris.jsp\"  >Form Inventaris</a></li>\n");
      out.write("                                                                <li><a href=\"Form-pengadaan.jsp\"  >Form Pengadaan</a></li>\n");
      out.write("\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<!--script-->\n");
      out.write("\t </div>\n");
      out.write("   </div>\n");
      out.write("</div>\n");
      out.write("<!--header end here-->\n");
      out.write("<!--about start here-->\n");
      out.write("<div class=\"about\">\n");
      out.write("\t<div class=\"container\">\n");
      out.write("\t\t<div class=\"about-main\">\n");
      out.write("\t\t\t    <div class=\"about-top\">\n");
      out.write("\t\t\t\t\t<h2>Daftar Procurement LPSI 2</h2>\n");
      out.write("\t\t\t\t\t<p>Laboratorium Pemrograman Sistem Informasi 2</p>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("                   \n");
      out.write("\t\t\t\t\n");
      out.write("\t\t</div>\n");
      out.write("\t\t</div>\n");
      out.write("   \n");
      out.write("    \n");
      out.write("    ");

        try {
//load jdbc driver
Class.forName("com.mysql.jdbc.Driver");

//connect to database
String url = "jdbc:mysql://localhost:3306/kpplbarokah";
Connection koneksi=DriverManager.getConnection(url, "root", "bismillah");


//display table
Statement stmt = koneksi.createStatement();
ResultSet result = stmt.executeQuery("select * from procurement");
 

      out.write("\n");
      out.write("<center> <table id=\"tbl-table\" border=\"3%\" width=\"86%\" class=\"order-table\">\n");
      out.write("\t\t\t\t\t<tr>\n");
      out.write("                                                <td width=\"10%\"><b><center>ID procurement</center></b></td>\n");
      out.write("                                                <td width=\"10%\"><b><center>ID Group Barang</center></b></td>\n");
      out.write("                                                <td width=\"10%\"><b><center>Kode Barang</center></b></td>\n");
      out.write("                                                <td width=\"10%\"><b><center>Nama Barang</center></b></td>\n");
      out.write("                                                <td width=\"10%\"><b><center>Jumlah Barang</center></b></td>\n");
      out.write("                                                <td width=\"10%\"><b><center>Harga Barang</center></b></td>\n");
      out.write("                                                <td width=\"10%\"><b><center>Tanggal Pesan</center></b></td>\n");
      out.write("                                                <td width=\"10%\"><b><center>Perusahaan Pemasok</center></b></td>\n");
      out.write("                                                  <td width=\"10%\"><b><center>Edit</center></b></td>\n");
      out.write("                                                <td width=\"8%\"><b><center>Delete</center></b></td>\n");
      out.write("                                                                         \n");
      out.write("\t\t\t\t\t\t\n");
      out.write("            </tr>\n");
      out.write("            ");
 while (result.next()){
            Object[] row={result.getString("No_pro"),result.getString("ID_grupbarang"),result.getString("kode_tiapbarang"),
                result.getString("nama_barang"),result.getString("jumlah_barang"),result.getString("harga_barang"),result.getString("tanggal_pesan"),
                result.getString("ID_pemasok")};
            
      out.write("\n");
      out.write("            \n");
      out.write("            <tr>\n");
      out.write("                <td><center>");
out.print(row[0]);
      out.write("</center></td>\n");
      out.write("                <td><center>");
out.print(row[1]);
      out.write("</center></td>\n");
      out.write("                <td><center>");
out.print(row[2]);
      out.write("</center></td>\n");
      out.write("                <td><center>");
out.print(row[3]);
      out.write("</center></td>\n");
      out.write("                <td><center>");
out.print(row[4]);
      out.write("</center></td>\n");
      out.write("                <td><center>");
out.print(row[5]);
      out.write("</center></td>\n");
      out.write("                <td><center>");
out.print(row[6]);
      out.write("</center></td>\n");
      out.write("                <td><center>");
out.print(row[7]);
      out.write("</center></td>            \n");
      out.write("                \n");
      out.write("                <td><center><a href=\"Formedit-pengadaan.jsp?id=");
out.print(String.valueOf(row[0]));
      out.write("\"><input type=\"button\" name=\"edit\" value=\"Edit\" onClick=\"javascript:window.location='Edit-inventaris.jsp';\" style=\"background-color:#49743D;font-weight:bold;color:#ffffff;\"></a> \n");
      out.write("                <td><a href=\"Delete-procurement.jsp?id=");
out.print(String.valueOf(row[0]));
      out.write("\"><center>\n");
      out.write("                      <input type=\"button\" name=\"delete\" value=\"Delete\" style=\"background-color:#49743D;font-weight:bold;color:#ffffff;\"></a>\n");
      out.write("                        \n");
      out.write("            </tr>\n");
      out.write("            ");
 } 
      out.write("\n");
      out.write("            ");

 result.close();
 stmt.close();
 koneksi.close();
 }
 catch (Exception ex)
 {
 out.println ("database not connected");
 }
 
 
      out.write("\n");
      out.write("    </table></center>\n");
      out.write("    <script type=\"text/javascript\">\n");
      out.write("  function doPrint() { window.print(); }\n");
      out.write("</script>\n");
      out.write("\n");
      out.write("<center><button onclick=\"doPrint();\">Print</button></center>  \n");
      out.write("</div>\n");
      out.write("<div class=\"copyright\">\n");
      out.write("\t<div class=\"container\">\n");
      out.write("\t\t<div class=\"copyright-main\">\n");
      out.write("\t\t\t   <p> Konstruksi Pemrograman Perangkat Lunak - A || Natascha, Unsa, Mia, Selina, Elisa </p> \n");
      out.write("\t\t\t<div class=\"clearfix\"> </div>\n");
      out.write("\t\t</div>\n");
      out.write("\t</div>\n");
      out.write("</div>\n");
      out.write("<!--copyright end here-->\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}

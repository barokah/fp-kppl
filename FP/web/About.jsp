<%-- 
    Document   : About
    Created on : Dec 26, 2015, 3:16:29 PM
    Author     : samsung
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
   <head>
       
<title>Inventaris Laboratorium Pemrograman Sistem Informasi</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.11.0.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Study Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--Google Fonts-->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
	</script>
<!-- //end-smoth-scrolling -->
<script src="js/menu_jquery.js"></script>
</head>
    <body>
<!--header start here-->
<div class="header1">
	<div class="container">
		 <div class="header-main">
                    <div class="header-logo">
                    <div class="logo">
                    <a href="index.html"><img src="images/lo1.png" alt="" ></a>
		    </div>
						<div class="top-nav">
							<span class="icon"><img src="images/menu.png" alt=""> </span>
							<ul>
                                                             <li ><a href="index.jsp">Home</a> </li>
                            <li ><a href="About.jsp" >About </a> </li>
								
								<li><a href="index.jsp"> <img src="images/sisfor.jpg" width="152" height="82"> </a></li>
                                                                <li><a href="Barang-inventaris.jsp" >Barang Inventaris</a></li>
								<li><a href="Contact.jsp" >Contact Admin </a></li>
                                                                <li><a href="Logout_user.jsp" >Logout</a></li>
							</ul>
							<!--script-->
						<script>
							$("span.icon").click(function(){
								$(".top-nav ul").slideToggle(500, function(){
								});
							});
					</script>				
				</div>
				<div class="clearfix"> </div>
					</div>
			<!---->
			<div class="top-menu">					
					<ul>
						   <li ><a href="index.jsp">Home</a> </li>
                    <li ><a href="About.jsp" >About </a> </li>
                    <li><a href="Contact.jsp" >Contact Admin </a></li>
                     <li><a href="index.jsp"> <img src="images/sisfor.jpg" width="152" height="82"> </a></li>
                    <li><a href="Login.jsp" >Login Admin</a></li>
                    <li><a href="Login_user.jsp" >Login User</a></li>
                    <li><a href="Signup.jsp" >Sign Up User</a></li>
						
					</ul>
				</div>
					<!--script-->
	 </div>
   </div>
</div>
<!--header end here-->
<!--about start here-->
<div class="about">
	<div class="container">
		<div class="about-main">
			    <div class="about-top">
					<h2>About LPSI 2</h2>
					<p>Laboratorium Pemrograman Sistem Informasi 2</p>
				</div>
				<div class="about-bottom">
					<div class="col-md-7 about-left">
						<h6><a href="single.html">Tentang Laboratorium Pemograman Sistem Informasi</a></h6>
						<p>Laboratorium Pemrograman Sistem Informasi merupakan laboratorium pemrograman yang berfungsi untuk membantu bagian akademis mahasiswa aktif sistem informasi ITS.Di Laboratorium Pemrograman Sistem Informasi 2, mahasiswa dapat mengerjakan tugas akademik dan melakukan praktikum untuk proses akademis. </p>
					</div>
					<div class="col-md-5 about-right">
						<a href="single.html" >
							<img src="images/lpsi2.jpg" alt="" class="img-responsive ">
						</a>								
					</div>
			</div>
		</div>
		</div>
</div>
<!--footer end here-->
<!--copyright start here-->
<div class="copyright">
	<div class="container">
		<div class="copyright-main">
			   <p> Konstruksi Pemrograman Perangkat Lunak - A || Natascha, Unsa, Mia, Selina, Elisa </p> 
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!--copyright end here-->
</body>
</html>

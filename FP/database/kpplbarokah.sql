CREATE DATABASE  IF NOT EXISTS `kpplbarokah` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `kpplbarokah`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: kpplbarokah
-- ------------------------------------------------------
-- Server version	5.1.73-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES ('rizky','admin'),('rowi','admin1');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `ID_kategori` int(10) NOT NULL AUTO_INCREMENT,
  `Jenis_kategori` varchar(100) NOT NULL,
  PRIMARY KEY (`ID_kategori`),
  KEY `ID_kategori` (`ID_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Inventaris'),(2,'Habis pakai');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grup_barang`
--

DROP TABLE IF EXISTS `grup_barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grup_barang` (
  `ID_grupbarang` varchar(50) NOT NULL,
  `nama_grupbarang` varchar(20) NOT NULL,
  `ID_kategori` int(10) NOT NULL,
  `kode_tiapbarang` varchar(15) NOT NULL,
  PRIMARY KEY (`ID_grupbarang`),
  KEY `ID_kategori` (`ID_kategori`),
  KEY `ID_kategori_2` (`ID_kategori`),
  KEY `kode_tiapbarang` (`kode_tiapbarang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grup_barang`
--

LOCK TABLES `grup_barang` WRITE;
/*!40000 ALTER TABLE `grup_barang` DISABLE KEYS */;
INSERT INTO `grup_barang` VALUES ('ACNTL','AC national',1,'ACNTL1'),('BRDWH','Whiteboard',1,'BRDWH1'),('CPU','CPU',1,'CPU1'),('KRSELV','Kursi elviron',1,'KRSELV1'),('KYBD','Keyboard',1,'KYBD1'),('LCDLG','Monitor LCD LG',1,'LCDLG1'),('LCDPRY','Layar proyektor',1,'LCDPRY1'),('LOCKBG','Locker',1,'LOCKBG1'),('MOS','Mouse',1,'MOS1'),('PHCHC','pengharum ruangan',2,'PHCHC1'),('PHTC','Proyektor Hitachi',1,'PHTC1'),('SPHTM','spidol ',2,'SPHTM1');
/*!40000 ALTER TABLE `grup_barang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventaris_habispakai`
--

DROP TABLE IF EXISTS `inventaris_habispakai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventaris_habispakai` (
  `kode_tiapbarang` varchar(15) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `harga_satuan` varchar(100) NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `batas_servis` int(10) DEFAULT NULL,
  `satuan_servis` varchar(10) DEFAULT NULL,
  `ID_pemasok` varchar(20) NOT NULL,
  `ID_kategori` int(10) NOT NULL,
  `Jumlah_barang` int(11) NOT NULL,
  `satuan_barang` varchar(11) NOT NULL,
  `status_barang` varchar(10) NOT NULL,
  PRIMARY KEY (`kode_tiapbarang`),
  KEY `ID_kategori` (`ID_kategori`),
  KEY `ID_pemasok` (`ID_pemasok`),
  KEY `ID_kategori_2` (`ID_kategori`),
  KEY `ID_kategori_3` (`ID_kategori`),
  KEY `ID_kategori_4` (`ID_kategori`),
  KEY `nama_barang` (`nama_barang`),
  KEY `nama_barang_2` (`nama_barang`),
  KEY `nama_barang_3` (`nama_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventaris_habispakai`
--

LOCK TABLES `inventaris_habispakai` WRITE;
/*!40000 ALTER TABLE `inventaris_habispakai` DISABLE KEYS */;
INSERT INTO `inventaris_habispakai` VALUES ('ACNTL1','AC national','4500000','2015-12-01',5,'tahun','pasok2',1,1,'unit','Rusak'),('BRDWH1','Whiteboard','300000','2015-12-02',5,'tahun','pasok3',1,1,'unit','Baik'),('CPU1','CPU','67000','2015-12-01',5,'tahun','Digitech',1,1,'unit',''),('ERSEBRD1','Penghapus papan','35000','2015-12-01',0,'','pasok4',2,1,'unit',''),('KRSELV1','Kursi Elviron','150000','2015-12-01',6,'tahun','pasok3',1,1,'unit',''),('KYBD1','Keyboard','250000','2015-12-01',5,'tahun','pasok1',1,1,'unit',''),('LCDLG1','Monitor LCD LG','500000','2015-12-01',3,'tahun','pasok1',1,1,'unit',''),('LCDPRY1','Layar proyektor','3500000','2015-12-01',3,'tahun','pasok1',1,1,'unit',''),('LMPPHL1','Lampu phillips','75000','2015-12-01',0,'','pasok2',2,1,'unit',''),('LOCKBG1','Locker','450000','2015-12-01',5,'tahun','pasok3',1,1,'unit',''),('MOS1','Mouse','200000','2015-12-01',5,'tahun','pasok1',1,1,'unit',''),('PHCHC1','Pengharum ruangan coklat','50000','2015-12-01',0,'','pasok4',2,1,'unit',''),('PHTC1','Proyektor Hitachi','3500000','2015-12-01',3,'tahun','pasok1',1,1,'unit','');
/*!40000 ALTER TABLE `inventaris_habispakai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maintain`
--

DROP TABLE IF EXISTS `maintain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maintain` (
  `kode_tiapbarang` varchar(15) NOT NULL,
  `tgl_mantain` date NOT NULL,
  `ID_maintain` int(15) NOT NULL AUTO_INCREMENT,
  `jenis_kerusakan` varchar(50) NOT NULL,
  `hasil_perbaikan` varchar(50) NOT NULL,
  `ID_grupbarang` varchar(50) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `jumlah_barang` int(10) NOT NULL,
  `status_barang` varchar(100) NOT NULL,
  PRIMARY KEY (`ID_maintain`),
  KEY `kode_tiapbarang` (`kode_tiapbarang`),
  KEY `nama_barang` (`nama_barang`),
  KEY `nama_barang_2` (`nama_barang`),
  KEY `nama_barang_3` (`nama_barang`),
  KEY `kode_tiapbarang_2` (`kode_tiapbarang`),
  KEY `kode_tiapbarang_3` (`kode_tiapbarang`),
  KEY `nama_barang_4` (`nama_barang`),
  KEY `nama_barang_5` (`nama_barang`),
  KEY `ID_grupbarang` (`ID_grupbarang`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maintain`
--

LOCK TABLES `maintain` WRITE;
/*!40000 ALTER TABLE `maintain` DISABLE KEYS */;
INSERT INTO `maintain` VALUES ('CPU1','2015-12-27',1,'motherboad rusak','perbaikan motherboard ','CPU','CPU',1,'Tahap maintain '),('KYBD1','2015-12-31',2,'keyboard tidak berfungsi','keyboard harus diganti yang baru','KYBD','keyboard',2,'Selesai'),('LCDLG1','2016-02-26',3,'tidak bisa fokus, lensa rusak','lensa diganti baru','LCDLG','Monitor LCD ',1,'Selesai'),('MOS1','2016-03-17',4,'Tidak berfungsi','harus diganti baru','MOS','Mouse',3,'Selesai'),('ACNTL1','2015-12-30',5,'AC bocor','Melakukan service','ACNTL','AC',1,'Tahap maintain');
/*!40000 ALTER TABLE `maintain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pemasok`
--

DROP TABLE IF EXISTS `pemasok`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pemasok` (
  `ID_pemasok` varchar(20) NOT NULL,
  `nama_pemasok` varchar(50) NOT NULL,
  `no_telepon` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_pemasok`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pemasok`
--

LOCK TABLES `pemasok` WRITE;
/*!40000 ALTER TABLE `pemasok` DISABLE KEYS */;
INSERT INTO `pemasok` VALUES ('pasok1','digitech','03177788890'),('pasok2','hartoni','0315557896'),('pasok3','Mebel Jaya','03172331095'),('pasok4','mawadah','0317444560');
/*!40000 ALTER TABLE `pemasok` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `procurement`
--

DROP TABLE IF EXISTS `procurement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procurement` (
  `No_pro` int(11) NOT NULL AUTO_INCREMENT,
  `ID_grupbarang` varchar(50) NOT NULL,
  `kode_tiapbarang` varchar(15) NOT NULL,
  `nama_barang` varchar(15) NOT NULL,
  `jumlah_barang` int(11) NOT NULL,
  `harga_barang` int(11) NOT NULL,
  `tanggal_pesan` date NOT NULL,
  `ID_pemasok` varchar(20) NOT NULL,
  PRIMARY KEY (`No_pro`),
  KEY `nama_barang` (`nama_barang`),
  KEY `ID_pemasok` (`ID_pemasok`),
  KEY `ID_grupbarang` (`ID_grupbarang`),
  KEY `nama_barang_2` (`nama_barang`),
  KEY `kode_tiapbarang` (`kode_tiapbarang`),
  KEY `nama_barang_3` (`nama_barang`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procurement`
--

LOCK TABLES `procurement` WRITE;
/*!40000 ALTER TABLE `procurement` DISABLE KEYS */;
INSERT INTO `procurement` VALUES (1,'ACNTL','ACNTL1','AC National',20,1500000,'2015-12-09','Digitech'),(2,'KYBD','KYBD1','keyboard',100,50000,'2015-12-01','Digitech');
/*!40000 ALTER TABLE `procurement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `nama_lengkap` varchar(20) NOT NULL,
  `nrp` varchar(9) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(10) NOT NULL,
  `email` varchar(20) NOT NULL,
  `no_telepon` varchar(20) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('Elisa Dian','12','dian','123','el@gmail.com','098'),('Elisa Dian','521310004','el','123','el@gmail.com','098765'),('Elisa','521310004','elisa','123','elisa13@mhs','0856'),('Mia','521310001','miaekas','lalala','miaekasetya@gmail.co','085743175297'),('selina','521310007','seliseli','seli','seli@gmail.com','77777');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-01 15:56:48
